-- Уверенности, что это самый оптимальный и быстрый способ - нет

select d.type, d.value from `data` d
inner join
 	(select type, max(date) as last_date from `data` group by type) as q
on
	d.type = q.type and d.date = q.last_date;