CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM;

ALTER TABLE `authors` ADD PRIMARY KEY (`id`);


CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM;

ALTER TABLE `books` ADD PRIMARY KEY (`id`);


CREATE TABLE `book_authors` (
  `book_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL
) ENGINE=MyISAM ;

ALTER TABLE `book_authors` ADD UNIQUE KEY `book_author` (`book_id`,`author_id`);

