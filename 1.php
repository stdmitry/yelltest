<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 30.09.15
 * Time: 13:06
 */

class DrawContext {
	function drawLine($coords, $params) {
		echo 'draw line'.PHP_EOL;
	}
	function drawCircle($coords, $params) {
		echo 'draw circle'.PHP_EOL;
	}
}

abstract class Shape {
	// width, color, coords, angle, etc.
	public $params;

	public function __construct($params) {
		$this->params = $params;
	}

	public function __toString() {
		return get_class($this);
	}

	/* @throws ShapeException */
	public static function create($params){
		return ShapeFabric::create($params);
	}

	abstract public function draw(DrawContext $dc);
	abstract public function getDrawData();
}

class Rectangle extends Shape {
	public function draw(DrawContext $dc) {
		$dc->drawLine(array(), array());
		$dc->drawLine(array(), array());
		$dc->drawLine(array(), array());
		$dc->drawLine(array(), array());
	}
	public function getDrawData() {
		return array(
			array('type' => 'line', 'params' => array()),
			array('type' => 'line', 'params' => array()),
			// ..
		);
	}
}

class Circle extends Shape {
	public function draw(DrawContext $dc) {
		$dc->drawCircle(array(), array());
	}
	public function getDrawData() {
		return array(
			array('type' => 'circle', 'params' => array()),
		);
	}
}

class SomeComplexShape extends Shape {
	private $shapes;
	public function draw (DrawContext $dc) {
		foreach ($this->shapes as $shape) {
			$shape->draw($dc);
		}
	}

	public function getDrawData() {
		$drawData = array();
		foreach($this->shapes as $shape) {
			$drawData = array_merge($drawData, $shape->drawData);
		}
		return $drawData;
	}
}

class ShapeException extends Exception { }

class ShapeFabric {
	static public function create($data) {
		if (!isset($data['type']))
			throw new ShapeException('Undefined format: type is missing');
		switch ($data['type']) {
			case 'circle' : return new Circle($data['params']);
			case 'rectangle': return new Rectangle($data['params']);
			case 'complex:': return new SomeComplexShape($data['params']);
		}
		throw new ShapeException('Undefined type: '.$data['type'].' is unknown');
	}
}

class Controller {

	public function main($shapesData) {

		$shapes = [];
		foreach ($shapesData as $data) {
			try {
				$shapes[] = Shape::create($data);
			}
			catch (ShapeException $ex) {
				echo $ex;
			}
		}
		$dc = new DrawContext();

		foreach ($shapes as $shape) {
			$shape->draw($dc);
			echo $shape.PHP_EOL;
			echo var_export($shape->getDrawData(), true).PHP_EOL;
		}
	}
}

$shapesData = [
	['type' => 'circle', 'params' => []],
	['type' => 'circle', 'params' => []],
	['type' => 'rectangle', 'params' => []],
	['type' => 'complex', 'params' => []],
	['type' => 'newType', 'params' => []],
	['type1' => 'wrongType', 'params' => []],
];

$ctrl = new Controller();
$ctrl->main($shapesData);

